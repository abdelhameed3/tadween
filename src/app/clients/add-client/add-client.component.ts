import { HelperService } from "./../../services/helper.service";
import { ClientsService } from "./../../services/clients.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-client",
  templateUrl: "./add-client.component.html",
  styleUrls: ["./add-client.component.scss"],
})
export class AddClientComponent implements OnInit {
  key;
  clients = [] as any;
  constructor(
    private clientsService: ClientsService,
    private helper: HelperService,
    private router: Router
  ) {}

  ngOnInit() {}

  async check() {
    try {
      this.clients = [];
      const res: any = await this.clientsService.get(this.key);
      if (res.status) {
        this.clients = res.data;
      } else {
        this.helper.errorAlert("Not Register");
      }
    } catch (err) {
      console.log(err);
    }
  }
  async addClient(id) {
    try {
      const res: any = await this.clientsService.add(id, null);
      if (res.status) {
        this.helper.successAlert("Client Added");
        this.router.navigate([`/user/list-clients`]);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

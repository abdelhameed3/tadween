import { environment } from "./../../../environments/environment";
import { HelperService } from "./../../services/helper.service";
import { ClientsService } from "./../../services/clients.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-list-clients",
  templateUrl: "./list-clients.component.html",
  styleUrls: ["./list-clients.component.scss"],
})
export class ListClientsComponent implements OnInit {
  @ViewChild("ClientInfo") ClientInfo: ModalDirective;
  @ViewChild("EditClient") EditClient: ModalDirective;
  @ViewChild("mapInfo") mapInfo: ModalDirective;
  @ViewChild("clientMapInfo") clientMapInfo: ModalDirective;
  @ViewChild("clientInvoices") clientInvoices: ModalDirective;
  publicUrl = environment.publicUrl;

  clients = [] as any;
  inovicesRes = {} as any;
  selectedClient = [] as any;
  mapCenter = {
    lat: 30.033333,
    lng: 31.233334,
  };

  mapCenter2 = {
    lat: 30.033333,
    lng: 31.233334,
  };

  lat;
  lng;
  zoom = 10;
  zoom2 = 10;

  markers: marker[] = [];
  markers2: marker[] = [];

  invoiceId;
  constructor(
    private clientsService: ClientsService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.getAllClients();
  }

  async getAllClients() {
    try {
      const res: any = await this.clientsService.getAll();
      console.log(res);
      if (res.status) {
        this.clients = res.data;
        res.data.map((el) => {
          this.markers.push({
            lat: el.latitude,
            lng: el.longitude,
            draggable: false,
          });
          this.mapCenter = {
            lat: +el.latitude,
            lng: +el.latitude,
          };
        });
      } else {
        this.helperService.errorAlert(res.massage);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async getInovicesRes(id) {
    try {
      this.invoiceId = id;
      this.selectedClient = this.clients.filter((el) => el.id == id);
      this.markers2 = [
        {
          lat: this.selectedClient[0].latitude,
          lng: this.selectedClient[0].longitude,
          draggable: false,
        },
      ];
      this.mapCenter2 = {
        lat: +this.selectedClient[0].latitude,
        lng: +this.selectedClient[0].longitude,
      };
      this.zoom2 = 5;
      const res: any = await this.clientsService.getInovices(id);
      if (res.status) {
        this.inovicesRes = res.data;
        this.ClientInfo.show();
      } else {
        this.helperService.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  showMap() {
    this.mapInfo.show();
  }
  showClientMap() {
    this.clientMapInfo.show();
  }
  showClientInvoices() {
    this.clientInvoices.show();
  }
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

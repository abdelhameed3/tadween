import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { AuthService } from "./../services/auth.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MessageService } from "../@pages/components/message/message.service";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"]
})
export class ResetPasswordComponent implements OnInit {
  public resetPasswordForm: FormGroup;

  constructor(
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _messageService: MessageService
  ) {}

  resetPassword() {
    const email = this.resetPasswordForm.controls.email.value;
    this._authService
      .resetPassword(email)
      .toPromise()
      .then(res => {
        this._router.navigate(["/"]);
        setTimeout(() => {
          this._messageService.create("success", res.msg, {
            Title: "Success",
            Position: "top-right",
            Style: "flip",
            Duration: 4000
          });
        });
      })
      .catch(err => {
        setTimeout(() => {
          this._messageService.create("error", err.error.msg, {
            Title: "Error",
            Position: "top-right",
            Style: "flip",
            Duration: 4000
          });
        });
      });
  }

  public ngOnInit() {
    this.resetPasswordForm = this._formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }
}

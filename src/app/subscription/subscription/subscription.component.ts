import { HelperService } from "./../../services/helper.service";
import { SubscribeService } from "./../../services/subscribe.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-subscription",
  templateUrl: "./subscription.component.html",
  styleUrls: ["./subscription.component.scss"],
})
export class SubscriptionComponent implements OnInit {
  subscribtionPlan;
  selectedPlan;
  constructor(
    private subscribeService: SubscribeService,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.getAll();
  }
  async getAll() {
    try {
      const res: any = await this.subscribeService.get();
      if (res.status) {
        this.subscribtionPlan = res.data;
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  selectPlan(id) {
    this.selectedPlan = id;
  }
  async subscribePlan() {
    try {
      const res: any = await this.subscribeService.add(this.selectedPlan);
      if (res.status) {
        this.subscribtionPlan = res.data;
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

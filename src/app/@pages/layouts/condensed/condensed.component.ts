import { environment } from "./../../../../environments/environment";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  HostListener,
  AfterViewInit,
  Input,
  ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { RootLayout } from "../root/root.component";
import { AuthService } from "../../../services/auth.service";

@Component({
  selector: "condensed-layout",
  templateUrl: "./condensed.component.html",
  styleUrls: ["./condensed.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CondensedComponent extends RootLayout implements OnInit {
  publicUrl = environment.publicUrl;

  loggedInUserName = "";
  menuLinks = [
    // {
    //   label: "User",
    //   iconType: "fas",
    //   iconName: "fa-user",
    //   toggle: "close",
    //   submenu: [
    //     {
    //       label: "Add User",
    //       routerLink: "/user/add-user",
    //       iconType: "fas",
    //       iconName: "fa-plus",
    //     },
    //     {
    //       label: "List User",
    //       routerLink: "/user/list-user",
    //       iconType: "fas",
    //       iconName: "fa-list",
    //     },
    //   ],
    // },
    {
      label: "Home",
      iconType: "fas",
      iconName: "fa-home",
      routerLink: "/user/dashboard",
    },
    {
      label: "Invoices",
      iconType: "fas",
      iconName: "fa-file-invoice",
      routerLink: "/user/list-invoices",
    },
    {
      label: "Clients",
      iconType: "fas",
      iconName: "fa-user",
      routerLink: "/user/list-clients",
    },
    {
      label: "Products",
      iconType: "fas",
      iconName: "fa-adjust",
      routerLink: "/user/list-products",
    },
    {
      label: "Representatives",
      iconType: "fas",
      iconName: "fa-address-card",
      routerLink: "/user/list-representatives",
    },
    {
      label: "Vouchers",
      iconType: "fas",
      iconName: "fa-ad",
      routerLink: "/user/list-vouchers",
    },
    {
      label: "Subscription",
      iconType: "fas",
      iconName: "fa-plus",
      routerLink: "/user/subscription",
    },
    {
      label: "Settings",
      iconType: "fas",
      iconName: "fa-cogs",
      routerLink: "/user/settings",
    },
    // {
    //   label: "Optimization",
    //   iconType: "fas",
    //   iconName: "fa-chart-line",
    //   routerLink: "/testing-platform/benchmark",
    // },
  ];
  Wholesaler;
  userPhoto;

  ngOnInit() {
    this.loggedInUserName = this.authService.loggedInInfo.userName;
    this.userPhoto =
      this.publicUrl + JSON.parse(localStorage.getItem("user")).photo;
  }
  isWholesaler() {
    return this.authService.isWholesaler();
  }
  userName = JSON.parse(localStorage.getItem("user")).name;

  logout() {
    localStorage.clear();
  }
}

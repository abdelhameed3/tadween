import { HelperService } from "./../services/helper.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as jwt from "jwt-decode";
import { AuthService } from "../services/auth.service";
import { UserRoleIdEnum } from "../enums/account-role.enum";
import { DropdownModule } from "primeng/dropdown";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  countries: any[];
  phone;
  password;
  inputPassword = true;
  user = {} as any;
  constructor(
    private authService: AuthService,
    private router: Router,
    private helper: HelperService
  ) {
    if (authService.isLoggedIn()) {
      router.navigate(["/user/dashboard"]);
    }
  }

  ngOnInit() {
    this.countries = [
      { flag: "../../../assets/img/egypt.svg", code: "+20", name: "+20" },
      { flag: "../../../assets/img/ksa.svg", code: "+996", name: "+996" },
    ];
  }

  async loginForm() {
    try {
      console.log(this.user);
      const formData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in this.user) {
        formData.append(key, this.user[key]);
      }
      const res: any = await this.authService.login(formData);
      console.log(res);
      if (res.status) {
        this.router.navigate(["/user/dashboard"]);
        localStorage.setItem("token", "Bearer " + res.data.api_token);
        localStorage.setItem("user", JSON.stringify(res.data.user));
        localStorage.setItem("type", JSON.stringify(res.data.user.type));

        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

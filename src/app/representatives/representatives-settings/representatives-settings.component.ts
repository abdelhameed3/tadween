import { HelperService } from "./../../services/helper.service";
import { ActivatedRoute } from "@angular/router";
import { RepresentativesService } from "./../../services/representatives.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-representatives-settings",
  templateUrl: "./representatives-settings.component.html",
  styleUrls: ["./representatives-settings.component.scss"],
})
export class RepresentativesSettingsComponent implements OnInit {
  representativesData = {} as any;
  representativeID;
  constructor(
    private representativesService: RepresentativesService,
    private activatedRoute: ActivatedRoute,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.representativeID = this.activatedRoute.snapshot.params.id;
    console.log(this.representativeID);
    this.getAll();
  }

  async getAll() {
    try {
      const res: any = await this.representativesService.getSetting(
        this.representativeID
      );
      if (res.status) {
        this.representativesData = res.data;
        if (this.representativesData.pay_now == 1) {
          this.representativesData.pay_now = true;
        } else {
          this.representativesData.pay_now = false;
        }

        if (this.representativesData.pay_later == 1) {
          this.representativesData.pay_later = true;
        } else {
          this.representativesData.pay_later = false;
        }
      }
    } catch (err) {
      console.log(err);
    }
  }

  async update() {
    try {
      if (this.representativesData.pay_now) {
        this.representativesData.pay_now = 1;
      } else {
        this.representativesData.pay_now = 0;
      }

      if (this.representativesData.pay_later) {
        this.representativesData.pay_later = 1;
      } else {
        this.representativesData.pay_later = 0;
      }
      const formData = this.helper.jsonToFormData(this.representativesData);
      const res: any = await this.representativesService.editSetting(
        this.representativeID,
        formData
      );
      if (res.status) {
        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
      console.log(this.representativesData);
    } catch (err) {
      console.log(err);
      this.helper.errorAlert(err.error.message);
    }
  }
}

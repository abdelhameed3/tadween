import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepresentativesSettingsComponent } from './representatives-settings.component';

describe('RepresentativesSettingsComponent', () => {
  let component: RepresentativesSettingsComponent;
  let fixture: ComponentFixture<RepresentativesSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepresentativesSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepresentativesSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

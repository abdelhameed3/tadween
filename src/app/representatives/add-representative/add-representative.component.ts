import { RepresentativesService } from "./../../services/representatives.service";
import { HelperService } from "./../../services/helper.service";
import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-add-representative",
  templateUrl: "./add-representative.component.html",
  styleUrls: ["./add-representative.component.scss"],
})
export class AddRepresentativeComponent implements OnInit {
  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  countries: any[];
  selectedCountry: any;
  representative = {} as any;
  phoneNumber;
  constructor(
    private sanitizer: DomSanitizer,
    private helper: HelperService,
    private representativesService: RepresentativesService
  ) {}

  ngOnInit() {
    this.countries = [
      { flag: "../../../assets/img/egypt.svg", code: "+20", name: "+20" },
      { flag: "../../../assets/img/ksa.svg", code: "+996", name: "+996" },
    ];
  }

  uploadFile($event) {
    console.log($event);
    this.fileData = $event.target.files[0];
    this.fileChanged = true;
    this.uploadedFile = this.sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(this.fileData)
    );
  }

  clearFile() {
    this.fileData = null;
    this.uploadedFile = null;
  }

  async addRepresentative() {
    try {
      this.representative.phone = this.selectedCountry.code + this.phoneNumber;
      if (this.fileData) {
        this.representative.photo = this.fileData;
      }
      console.log(this.representative);
      const formData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in this.representative) {
        formData.append(key, this.representative[key]);
      }
      const res: any = await this.representativesService.add(formData);
      console.log(res);
      if (res.status) {
        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

import { environment } from "./../../../environments/environment";
import { RepresentativesService } from "./../../services/representatives.service";
import { HelperService } from "./../../services/helper.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild } from "@angular/core";
@Component({
  selector: "app-list-representatives",
  templateUrl: "./list-representatives.component.html",
  styleUrls: ["./list-representatives.component.scss"],
})
export class ListRepresentativesComponent implements OnInit {
  @ViewChild("RepresentInfo") RepresentInfo: ModalDirective;
  @ViewChild("Image") Image: ModalDirective;

  representatives;
  publicUrl = environment.publicUrl;
  modalImage;
  details = {} as any;
  topClients = {} as any;
  representativesInvoices = {} as any;
  constructor(
    private helper: HelperService,
    private representativesService: RepresentativesService
  ) {}

  ngOnInit() {
    this.getRepresentatives();
  }
  async showrepresen(id) {
    try {
      const res: any = await this.representativesService.getById(id);
      const res2: any = await this.representativesService.getTop(id);
      const res3: any = await this.representativesService.getInvoices(id);
      this.representativesInvoices = res3.data;
      this.topClients = res2.data;
      this.details = res.data;
      if (res.status) {
        this.RepresentInfo.show();
      } else {
        this.helper.errorAlert(res.message);
      }
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  }
  async getRepresentatives() {
    try {
      const res: any = await this.representativesService.get();
      this.representatives = res.data;
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  }

  showImage(src) {
    console.log(src);
    this.modalImage = src;
    this.Image.show();
  }
}

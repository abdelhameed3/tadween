import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class SubscribeService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  get() {
    return this.http.get(`${this.apiUrl}/subscription`).toPromise();
  }
  add(id) {
    return this.http
      .post(`${this.apiUrl}/request-subscription/${id}`, null)
      .toPromise();
  }
}

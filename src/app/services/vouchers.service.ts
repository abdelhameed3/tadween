import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class VouchersService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(`${this.apiUrl}/receipt-vouchers`).toPromise();
  }
  listBanks() {
    return this.http.get(`${this.apiUrl}/list-of-banks`).toPromise();
  }
  notPaid(id) {
    return this.http.get(`${this.apiUrl}/not-paid-invoices/${id}`).toPromise();
  }

  add(body) {
    return this.http.post(`${this.apiUrl}/receipt-vouchers`, body).toPromise();
  }
}

import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getDropdowns() {
    return this.http.get(`${this.apiUrl}/account/create-account-dropdowns`);
  }

  createAccount(accountObject) {
    return this.http.post(`${this.apiUrl}/account/create-account`, accountObject);
  }

  viewAccount(accountId) {
    return this.http.get(`${this.apiUrl}/account/view-account/${accountId}`);
  }

  susspendAccount(account) {
    return this.http.post(`${this.apiUrl}/account/suspend-account`, account)
  }

  updateAccount(account) {
    return this.http.put(`${this.apiUrl}/account/update-account`, account)
  }

  viewUser(id) {
    return this.http.get(`${this.apiUrl}/user/${id}`);
  }

  getAccountModules() {
    return this.http.get(`${this.apiUrl}/account/account-modules`);
  }





}

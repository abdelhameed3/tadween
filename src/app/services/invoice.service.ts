import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class InvoiceService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  get() {
    return this.http.get(`${this.apiUrl}/invoices`).toPromise();
  }
  add(body) {
    return this.http
      .post(`${this.apiUrl}/wholesale-seller/orders-create/`, body)
      .toPromise();
  }
  getClients() {
    return this.http.get(`${this.apiUrl}/my_clients`).toPromise();
  }

  getById(id) {
    return this.http.get(`${this.apiUrl}/invoices/${id}`).toPromise();
  }
}

import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ClientsService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  get(key) {
    return this.http
      .get(`${this.apiUrl}/users?keyword=${key}&type=Retailer`)
      .toPromise();
  }
  add(id, body) {
    return this.http
      .post(`${this.apiUrl}/add-or-remove-client/${id}`, body)
      .toPromise();
  }
  getAll() {
    return this.http.get(`${this.apiUrl}/my-clients`).toPromise();
  }
  getInovices(id) {
    return this.http.get(`${this.apiUrl}/stores/${id}/summary`).toPromise();
  }
}

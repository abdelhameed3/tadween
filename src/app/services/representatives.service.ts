import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class RepresentativesService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  get() {
    return this.http.get(`${this.apiUrl}/stores/representatives`).toPromise();
  }
  add(body) {
    return this.http.post(`${this.apiUrl}/representative`, body).toPromise();
  }
  getTop(id) {
    return this.http
      .get(`${this.apiUrl}/top-client-representative/${id}`)
      .toPromise();
  }
  getInvoices(id) {
    return this.http
      .get(`${this.apiUrl}/list-invoices-count-by-representative/${id}`)
      .toPromise();
  }

  getById(id) {
    return this.http.get(`${this.apiUrl}/users/${id}`).toPromise();
  }
  getSetting(id) {
    return this.http
      .get(`${this.apiUrl}/representative-setting/${id}`)
      .toPromise();
  }
  editSetting(id, body) {
    return this.http
      .put(`${this.apiUrl}/update-representative-setting/${id}`, body)
      .toPromise();
  }
}

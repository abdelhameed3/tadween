import { Injectable } from "@angular/core";
import Swal from "sweetalert2";

@Injectable({
  providedIn: "root",
})
export class HelperService {
  constructor() {}
  validateFields(fileds: any[], containerObject: any) {
    let error = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < fileds.length; i++) {
      if (!containerObject[fileds[i].name]) {
        error = true;
        this.errorAlert(`${fileds[i].guiName} is required`);
        break;
      }
    }
    return error;
  }

  askBeforeDelete() {}

  buildFormData(formData, data, parentKey?) {
    if (
      data &&
      typeof data === "object" &&
      !(data instanceof Date) &&
      !(data instanceof File)
    ) {
      Object.keys(data).forEach((key) => {
        this.buildFormData(
          formData,
          data[key],
          parentKey ? `${parentKey}[${key}]` : key
        );
      });
    } else {
      const value = data == null ? "" : data;
      formData.append(parentKey, value);
    }
  }

  jsonToFormData(data) {
    const formData = new FormData();
    this.buildFormData(formData, data);
    return formData;
  }
  errorAlert(msg) {
    Swal.fire({
      icon: "error",
      title: msg,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  successAlert(msg) {
    Swal.fire({
      icon: "success",
      title: msg,
      showConfirmButton: false,
      timer: 1500,
    });
  }

  get currentUser() {
    return JSON.parse(localStorage.getItem("token"));
  }

  validateEmail(email) {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}

import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAccounts() {
    return this.http.get(`${this.apiUrl}/account/accounts-list`);
  }
  
  getUserList() {
    return this.http.get(`${this.apiUrl}/user/all`);
  }
}

import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  get(id) {
    return this.http.get(`${this.apiUrl}/store/products/${id}`).toPromise();
  }
  add(body) {
    return this.http.post(`${this.apiUrl}/products`, body).toPromise();
  }
  edit(id, body) {
    return this.http.post(`${this.apiUrl}/products/${id}`, body).toPromise();
  }
  getById(id) {
    return this.http.get(`${this.apiUrl}/products/${id}`).toPromise();
  }
}

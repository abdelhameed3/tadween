import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserRoleIdEnum } from "../enums/account-role.enum";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  login(body) {
    return this.http.post(`${this.apiUrl}/login`, body).toPromise();
  }

  createAdminOrUser(userName, email, phoneNumber, roleId, accountId, modules) {
    return this.http.post(`${this.apiUrl}/user/create-user`, {
      userName,
      email,
      phoneNumber,
      roleId,
      modules,
    });
  }

  createAccount(body) {
    return this.http.post(`${this.apiUrl}/register`, body).toPromise();
  }
  findRepresentative(phone) {
    return this.http
      .get(`${this.apiUrl}/find-representative-account?phone=${phone}`)
      .toPromise();
  }
  complete(id, body) {
    return this.http
      .post(`${this.apiUrl}/complete-your-profile/${id}`, body)
      .toPromise();
  }
  type() {
    return this.http.get(`${this.apiUrl}/user-types`).toPromise();
  }
  verify(body) {
    return this.http.post(`${this.apiUrl}/verify-account`, body).toPromise();
  }
  step3(body) {
    return this.http.post(`${this.apiUrl}/register-step3`, body).toPromise();
  }
  step2(body) {
    return this.http.post(`${this.apiUrl}/register-step2`, body).toPromise();
  }
  /**
   * Send Email to User Include Reset Password ID
   */
  public resetPassword(email: string): Observable<any> {
    return this.http.post<Observable<any>>(
      `${this.apiUrl}/auth/reset-password`,
      { email }
    );
  }

  /**
   * Send New Password and Reset Password ID to Change User Password
   * Note: Reset Password ID Expires in 3 Hours
   */
  public changePassword(
    newPassword: string,
    resetPasswordId: string
  ): Observable<any> {
    return this.http.post<Observable<any>>(
      `${this.apiUrl}/auth/change-password`,
      { newPassword, resetPasswordId }
    );
  }

  logout() {
    localStorage.removeItem("token");
  }

  isLoggedIn() {
    if (localStorage.getItem("token")) {
      return true;
    }
    return false;
  }

  isWholesaler() {
    if (
      JSON.parse(localStorage.getItem("user")).type == UserRoleIdEnum.Wholesaler
    ) {
      return true;
    }
    return false;
  }

  isRetailer() {
    if (
      JSON.parse(localStorage.getItem("user")).type == UserRoleIdEnum.Retailer
    ) {
      return true;
    }
    return false;
  }

  isRepresntative() {
    if (
      JSON.parse(localStorage.getItem("user")).type ==
      UserRoleIdEnum.Represntative
    ) {
      return true;
    }
    return false;
  }

  get loggedInInfo() {
    return JSON.parse(localStorage.getItem("user"));
  }
}

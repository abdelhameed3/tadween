import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  get(id) {
    return this.http.get(`${this.apiUrl}/users/${id}`).toPromise();
  }
  update(body) {
    return this.http.post(`${this.apiUrl}/update-profile`, body).toPromise();
  }
}

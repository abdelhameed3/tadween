import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class HomeService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  get() {
    return this.http.get(`${this.apiUrl}/home-statistics`).toPromise();
  }
}

import { HomeService } from "./../services/home.service";
import { AuthService } from "./../services/auth.service";
import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { DashboardService } from "../services/dashboard.service";
import { AccountService } from "../services/account.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
  @ViewChild("mdSlideUp") mdSlideUp: ModalDirective;
  @ViewChild(DatatableComponent) superAdminTable: DatatableComponent;
  @ViewChild(DatatableComponent) adminTable: DatatableComponent;

  userType = "Super Admin";
  // userType = 'Admin';

  superAdminAccounts = false;
  adminAccounts = false;

  superAdminTableRows = [];
  adminTableRows = [];

  suspended = [];
  accountType;

  basicSuperAdminRows = [];
  basicSuperAdminSort = [];

  basicAdminRows = [];
  basicAdminSort = [];

  showCreateIcon = false;

  homeData = {} as any;
  // scrollBarHorizontal = window.innerWidth < 960;
  // columnModeSetting = "force";
  // columnModeSettingSmall = "force";

  constructor(
    private homeService: HomeService,
    private accountService: AccountService,
    private _router: Router,
    public authService: AuthService
  ) {
    // window.onresize = () => {
    //   this.scrollBarHorizontal = window.innerWidth < 960;
    //   this.columnModeSetting = "force";
    //   this.columnModeSettingSmall = "force";
    // };
  }

  ngOnInit() {
    // console.log(this.authService.isSuperAdmin());
    // if(this.authService.isSuperAdmin()) {
    //   this.fetchSuperAdminData();
    // } else {
    //   this.fetchAdminData();
    // }
    this.getData();
  }

  async getData() {
    try {
      const res: any = await this.homeService.get();
      if (res.status) {
        this.homeData = res.data;
      }
    } catch (err) {
      console.log(err);
    }
  }

  // fetchSuperAdminData() {
  //   this.dashbaordService.getAccounts().subscribe(
  //     (res: any) => {
  //       console.log(res);
  //       if (res.data.length) {
  //         this.superAdminAccounts = true;
  //       } else {
  //         this.showCreateIcon = true;
  //       }
  //       res.data.forEach((el) => {
  //         el.active = true ? el.accountStatus === "active" : false;
  //         console.log(el);
  //       });
  //       this.superAdminTableRows = res.data;
  //       this.basicSuperAdminSort = [...res.data];
  //       // push our inital complete list
  //       this.basicSuperAdminRows = res.data;
  //     },
  //     (err) => {
  //       console.log(err);
  //     }
  //   );
  // }

  // fetchAdminData() {
  //   this.dashbaordService.getUserList().subscribe(
  //     (res: any) => {
  //       console.log(res);
  //       if (res.data.length) {
  //         this.adminAccounts = true;
  //       } else {
  //         this.showCreateIcon = true;
  //       }
  //       // res.data.forEach((el) => {
  //       //   el.active = true ? el.accountStatus === 'active' : false;
  //       //   console.log(el)
  //       // });
  //       this.adminTableRows = res.data;
  //       this.basicAdminSort = [...res.data];
  //       // push our inital complete list
  //       this.basicAdminRows = res.data;
  //     },
  //     (err) => {
  //       console.log(err);
  //     }
  //   );
  // }
}

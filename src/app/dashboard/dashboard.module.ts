//Angular Dependencies
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../@pages/components/shared.module";

import { pgCardModule } from "../@pages/components/card/card.module";
import { pgSelectModule } from "../@pages/components/select/select.module";
import { pgSwitchModule } from "../@pages/components/switch/switch.module";
import { pgTabsModule } from "../@pages/components/tabs/tabs.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { pgDatePickerModule } from "../@pages/components/datepicker/datepicker.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ModalModule } from "ngx-bootstrap";
import { ButtonsModule } from "ngx-bootstrap/buttons";

import {
  SwiperModule,
  SWIPER_CONFIG,
  SwiperConfigInterface,
} from "ngx-swiper-wrapper";

import { DashboardComponent } from "./dashboard.component";

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: "horizontal",
  slidesPerView: "auto",
};

const components = [DashboardComponent];

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    pgCardModule,
    pgTabsModule,
    SwiperModule,
    pgSwitchModule,
    pgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    pgDatePickerModule,
    NgxDatatableModule,
    ModalModule.forRoot(),
    ButtonsModule,
  ],
  declarations: components,
  exports: components,
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class DashboardModule {}

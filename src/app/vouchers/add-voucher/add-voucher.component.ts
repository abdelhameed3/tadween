import { InvoiceService } from "./../../services/invoice.service";
import { environment } from "./../../../environments/environment";
import { ClientsService } from "./../../services/clients.service";
import { HelperService } from "./../../services/helper.service";
import { VouchersService } from "./../../services/vouchers.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ModalDirective } from "ngx-bootstrap/modal";

@Component({
  selector: "app-add-voucher",
  templateUrl: "./add-voucher.component.html",
  styleUrls: ["./add-voucher.component.scss"],
})
export class AddVoucherComponent implements OnInit {
  @ViewChild("SelectClient") SelectClient: ModalDirective;
  @ViewChild("InvoiceInfo") InvoiceInfo: ModalDirective;

  publicUrl = environment.publicUrl;

  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  countries = [];
  // banks = [];
  selectedCountry: any;
  voucher = {
    type: "check",
    invoices: [],
  } as any;
  value: Date;
  namePrep;
  stores = [] as any;
  selectedStore = {} as any;
  min;
  inovices = [] as any;
  inovice = {} as any;
  totlePrice = 0;
  constructor(
    private sanitizer: DomSanitizer,
    private vouchersService: VouchersService,
    private clientsService: ClientsService,
    private helper: HelperService,
    private invoiceService: InvoiceService
  ) {}

  ngOnInit() {
    this.getBanks();
    this.min = new Date();
    this.namePrep = JSON.parse(localStorage.getItem("user")).name;
    this.inovices.length = 0;
    this.inovice = {
      id: 133019745,
      invoice_id: "168",
      totalQty: "5",
      pricing: {
        original_price: "10",
        total_price: "11.5",
        tax: "15%",
        receipt_vouchers_amount: "0",
        receipt_vouchers_amount_will_paid: "11.5",
      },
      payment_method: "Pay now",
      approval: "approved",
      payment_status: "unpaid",
      status: "approved",
      is_assigned: false,
      created_at: "2020/11/05",
      products: [
        {
          id: 542,
          name: "نوفا",
          photo: "/assets/images/products/1603280288file.jpeg",
          qty: 5,
          subtotal: 10,
          total: 10,
          price: 2,
        },
      ],
      user: {
        id: 61,
        name: "مسوق 1 ت",
        photo: "/assets/images/users/default.png",
        phone: "+201111111113",
        branch_id: 1,
        commercial_registered: "1111111113",
      },
      creator: {
        id: 47,
        name: "مسوق1",
        photo: "/assets/images/users/default.png",
        phone: "+201111111111",
        branch_id: 1,
        commercial_registered: "1111111111",
      },
      payment_date: "05/11/2020",
      wholesale_seller: {
        id: 47,
        name: "مسوق1",
        photo: "/assets/images/users/default.png",
        phone: "+201111111111",
        branch_id: 1,
        commercial_registered: "1111111111",
      },
      store: {
        id: 33,
        name: "مسوق1",
        store_responsible_name: "مسوق1",
        store_owner_name: "مسوق1",
        photo: "/assets/images/stores/",
        address: "",
      },
      company_logo:
        "/assets/images/store-setting/1602718738IMG_٢٠٢٠١٠١٥_٠٢٣٨٥٩٧٧١.jpg",
      company_name: "مسوق1",
    };
  }
  uploadFile($event) {
    console.log($event);
    this.fileData = $event.target.files[0];
    this.fileChanged = true;
    this.uploadedFile = this.sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(this.fileData)
    );
  }

  clearFile() {
    this.fileData = null;
    this.uploadedFile = null;
  }
  async ShowStoresModal() {
    try {
      const res: any = await this.clientsService.getAll();
      console.log(res);
      if (res.status) {
        this.stores = res.data;
        this.SelectClient.show();
      } else {
        this.helper.errorAlert(res.massage);
      }
    } catch (err) {
      console.log(err);
    }
  }

  getStoreId(id, item) {
    this.voucher.reciever_from = id;
    this.getInovices(this.voucher.reciever_from);
    this.selectedStore = item;
  }
  check() {
    if (this.voucher.amount > this.totlePrice) {
      this.voucher.amount = null;
    }
  }

  async getInovices(id) {
    try {
      this.totlePrice = 0;
      const res: any = await this.vouchersService.notPaid(id);
      this.inovices = res.data;
      this.inovices.map((el) => {
        this.voucher.invoices.push(el.id);
        this.totlePrice += +el.pricing.total_price;
      });
      console.log(this.totlePrice);
      this.SelectClient.hide();
    } catch (err) {
      console.log(err);
    }
  }
  async showInvoices(id) {
    try {
      const res: any = await this.invoiceService.getById(id);
      if (res.status) {
        this.inovice = res.data;
        this.InvoiceInfo.show();
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async getBanks() {
    try {
      const res: any = await this.vouchersService.listBanks();
      res.data.map((el) => {
        this.countries.push({ name: el, code: el });
      });
      console.log(this.countries);
    } catch (err) {
      console.log(err);
    }
  }
  async create() {
    try {
      if (this.selectedCountry) {
        this.voucher.bank_name = this.selectedCountry.code;
      }
      if (this.fileData) {
        this.voucher.check_image = this.fileData;
      }
      if (this.voucher.type == "check") {
        delete this.voucher.transfer_reference_number;
        delete this.voucher.transaction_number;
      }
      if (this.voucher.type == "card") {
        delete this.voucher.transfer_reference_number;
        delete this.voucher.bank_name;
        delete this.voucher.check_image;
        delete this.voucher.check_number;
      }
      if (this.voucher.type == "transfer") {
        delete this.voucher.transaction_number;
        delete this.voucher.bank_name;
        delete this.voucher.check_image;
        delete this.voucher.check_number;
      }
      this.voucher.prepared_by = JSON.parse(localStorage.getItem("user")).id;
      console.log(this.voucher);
      // const formData = new FormData();
      // // tslint:disable-next-line: forin
      // for (const key in this.voucher) {
      //   formData.append(key, this.voucher[key]);
      // }
      const res: any = await this.vouchersService.add(
        this.helper.jsonToFormData(this.voucher)
      );
      if (res.status) {
        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

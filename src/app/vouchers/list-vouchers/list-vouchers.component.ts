import { environment } from "./../../../environments/environment";
import { HelperService } from "./../../services/helper.service";
import { VouchersService } from "./../../services/vouchers.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-list-vouchers",
  templateUrl: "./list-vouchers.component.html",
  styleUrls: ["./list-vouchers.component.scss"],
})
export class ListVouchersComponent implements OnInit {
  @ViewChild("VoucherInfo") VoucherInfo: ModalDirective;
  @ViewChild("filterModal") filterModal: ModalDirective;
  fileChanged = false;
  countries: any[];
  selectedCountry: any;
  vouchers = [] as any;
  selectedVouchers = [] as any;
  publicUrl = environment.publicUrl;

  ngOnInit() {
    this.countries = [
      { name: "Australia", code: "AU" },
      { name: "Brazil", code: "BR" },
      { name: "China", code: "CN" },
      { name: "Egypt", code: "EG" },
      { name: "France", code: "FR" },
      { name: "Germany", code: "DE" },
      { name: "India", code: "IN" },
      { name: "Japan", code: "JP" },
      { name: "Spain", code: "ES" },
      { name: "United States", code: "US" },
    ];
    this.getAllVoucher();
  }
  
  constructor(
    private vouchersService: VouchersService,
    private helperService: HelperService
  ) {}

  
  showVoucher(id) {
    this.selectedVouchers = this.vouchers.filter((el) => el.id == id);
    this.VoucherInfo.show();
  }
  openFilter() {
    this.filterModal.show();
  }

  async getAllVoucher() {
    try {
      const res: any = await this.vouchersService.getAll();
      if (res.status) {
        this.vouchers = res.data;
        console.log(this.vouchers);
      } else {
        this.helperService.errorAlert(res.massage);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

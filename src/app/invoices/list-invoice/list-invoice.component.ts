import { HelperService } from "./../../services/helper.service";
import { InvoiceService } from "./../../services/invoice.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild } from "@angular/core";
import { environment } from "./../../../environments/environment";

@Component({
  selector: "app-list-invoice",
  templateUrl: "./list-invoice.component.html",
  styleUrls: ["./list-invoice.component.scss"],
})
export class ListInvoiceComponent implements OnInit {
  @ViewChild("filterModal") filterModal: ModalDirective;
  @ViewChild("InvoiceInfo") InvoiceInfo: ModalDirective;
  inovices = [] as any;
  publicUrl = environment.publicUrl;
  countries: any[];
  selectedCountry: any;

  inovice = {} as any;
  filterData = {} as any;
  constructor(
    private invoiceService: InvoiceService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.countries = [
      { name: "Australia", code: "AU" },
      { name: "Brazil", code: "BR" },
      { name: "China", code: "CN" },
      { name: "Egypt", code: "EG" },
      { name: "France", code: "FR" },
      { name: "Germany", code: "DE" },
      { name: "India", code: "IN" },
      { name: "Japan", code: "JP" },
      { name: "Spain", code: "ES" },
      { name: "United States", code: "US" },
    ];
    this.getInvoices();
    this.inovice = {
      id: 133019745,
      invoice_id: "168",
      totalQty: "5",
      pricing: {
        original_price: "10",
        total_price: "11.5",
        tax: "15%",
        receipt_vouchers_amount: "0",
        receipt_vouchers_amount_will_paid: "11.5",
      },
      payment_method: "Pay now",
      approval: "approved",
      payment_status: "unpaid",
      status: "approved",
      is_assigned: false,
      created_at: "2020/11/05",
      products: [
        {
          id: 542,
          name: "نوفا",
          photo: "/assets/images/products/1603280288file.jpeg",
          qty: 5,
          subtotal: 10,
          total: 10,
          price: 2,
        },
      ],
      user: {
        id: 61,
        name: "مسوق 1 ت",
        photo: "/assets/images/users/default.png",
        phone: "+201111111113",
        branch_id: 1,
        commercial_registered: "1111111113",
      },
      creator: {
        id: 47,
        name: "مسوق1",
        photo: "/assets/images/users/default.png",
        phone: "+201111111111",
        branch_id: 1,
        commercial_registered: "1111111111",
      },
      payment_date: "05/11/2020",
      wholesale_seller: {
        id: 47,
        name: "مسوق1",
        photo: "/assets/images/users/default.png",
        phone: "+201111111111",
        branch_id: 1,
        commercial_registered: "1111111111",
      },
      store: {
        id: 33,
        name: "مسوق1",
        store_responsible_name: "مسوق1",
        store_owner_name: "مسوق1",
        photo: "/assets/images/stores/",
        address: "",
      },
      company_logo:
        "/assets/images/store-setting/1602718738IMG_٢٠٢٠١٠١٥_٠٢٣٨٥٩٧٧١.jpg",
      company_name: "مسوق1",
    };
  }

  async getInvoices() {
    try {
      const res: any = await this.invoiceService.get();
      if (res.status) {
        this.inovices = res.data;
        console.log(res.data);
      } else {
        this.helperService.errorAlert(res.status);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async showInvoices(id) {
    try {
      const res: any = await this.invoiceService.getById(id);
      if (res.status) {
        this.inovice = res.data;
        this.InvoiceInfo.show();
      } else {
        this.helperService.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  filterInvoice() {
    console.log(this.filterData);
  }
  openFilter() {
    this.filterModal.show();
  }
}

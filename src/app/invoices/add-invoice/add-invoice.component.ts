import { Router } from "@angular/router";
import { ClientsService } from "./../../services/clients.service";
import { ProductService } from "./../../services/product.service";
import { HelperService } from "./../../services/helper.service";
import { InvoiceService } from "./../../services/invoice.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild } from "@angular/core";
import { environment } from "./../../../environments/environment";

@Component({
  selector: "app-add-invoice",
  templateUrl: "./add-invoice.component.html",
  styleUrls: ["./add-invoice.component.scss"],
})
export class AddInvoiceComponent implements OnInit {
  @ViewChild("ShowStores") ShowStores: ModalDirective;
  @ViewChild("SelectProduct") SelectProduct: ModalDirective;
  publicUrl = environment.publicUrl;

  step = 1;
  min;
  invoice = {
    method: "Pay now",
    discount_type: "value",
    products: [],
  } as any;
  datePayment;
  products = [] as any;
  addedProduect = [] as any;
  sendProduct;
  clientId;
  stores = [] as any;
  selectedStore = {} as any;
  constructor(
    private invoiceService: InvoiceService,
    private helperService: HelperService,
    private productService: ProductService,
    private clientsService: ClientsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.min = new Date();
  }
  async ShowStoresModal() {
    try {
      const res: any = await this.clientsService.getAll();
      console.log(res);
      if (res.status) {
        this.stores = res.data;
        this.ShowStores.show();
      } else {
        this.helperService.errorAlert(res.massage);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async ShowProducts() {
    try {
      let id = JSON.parse(localStorage.getItem("user")).store_id;
      const res: any = await this.productService.get(id);
      console.log(res.data);
      if (res.status) {
        this.products = res.data;
        this.products.map((el) => {
          el.qty = 1;
          el.old_price = el.price;
        });
        this.SelectProduct.show();
      } else {
        this.helperService.errorAlert(res.massage);
      }
    } catch (err) {
      console.log(err);
    }
  }

  showStep1() {
    this.step = 1;
  }
  showStep2() {
    this.step = 2;
  }
  addProduct() {
    this.addedProduect = this.products.filter((el) => el.checked);
    console.log(this.addedProduect);
    this.SelectProduct.hide();
  }
  deletFromArray(index) {
    this.addedProduect.splice(index, 1);
  }
  create() {
    if (this.datePayment && this.invoice.method == "Pay later") {
      let date = this.datePayment;
      let dd = String(date.getDate()).padStart(2, "0");
      let mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      let yyyy = date.getFullYear();
      date = mm + "/" + dd + "/" + yyyy;
      this.invoice.payment_date = date;
    }
    if (this.invoice.method == "Pay now") {
      delete this.invoice.payment_date;
    }

    this.addedProduect.map((el) => {
      this.invoice.products.push({
        price: el.price + "",
        qty: +el.qty,
        id: el.id + "",
      });
    });

    this.invoice.user_id = this.clientId;
    console.log(this.invoice);
    this.step = 2;
  }
  getStoreId(id, item) {
    this.clientId = id;
    this.selectedStore = item;
    console.log(this.selectedStore);
    this.ShowStores.hide();
  }
  async confirm() {
    try {
      const res: any = await this.invoiceService.add(this.invoice);
      if (res.status) {
        this.helperService.successAlert(res.message);
        this.router.navigate(["/user/list-invoices"]);
      } else {
        this.helperService.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

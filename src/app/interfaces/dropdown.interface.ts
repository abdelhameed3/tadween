export interface IDropDown {
  chartTypes: IDropDownElement[];
  deviceTypes: IDropDownElement[];
  events: IDropDownElement[];
  exportExtension: IDropDownElement[];
  imsis: IDropDownElement[];
  iotBoards: IDropDownElement[];
  kitType: IDropDownElement[];
  kpis: IDropDownElement[];
  mapType: IDropDownElement[];
  module: IDropDownElement[];
  moduleFeature: IDropDownElement[];
  technologyType: IDropDownElement[];
  videoQuality: IDropDownElement[];
  voiceQuality: IDropDownElement[];
}

export interface IDropDownElement {
  name: string;
  id?: number;
}

export interface IAccount {
  account: IAccountObject;
  chartTypes: []; // array of chart type ids to be inserted into ChartTypeLicense
  deviceTypes: []; // array of device type ids to be inserted into deviceTypeLicense
  events: []; // array of events ids to be inserted into eventsLicense
  voiceQuality: []; // array of voiceQuality ids to be inserted into voiceQualityLicense
  videoQuality: []; // array of videoQuality ids to be inserted into videoQualityLicense
  mapTypes: []; // array of map types ids to be inserted into mapTypeLicense
  technologyTypes: []; // array of technology type ids to be inserted into technologyTypesLicense
  exportExtensions: []; // array of exportExtensions ids to be inserted into exportExtensionsLicense
  iotBoards: []; // array of iotBoards ids to be inserted into iotBoardsLicense
  kitsTypes: []; // array of kitsTypes ids to be inserted into kitsTypeLicense
  kpis: []; // array of kpis ids to be inserted into kpisLicense
  moduleFeatures: []; // array of moduleFeature ids to be inserted into moduleFeatureLicense
  modules: [];
  IMSIS: [];
  admin: IAdmin;
}

export interface IAdmin {
  userName: string;
  email: string;
  phoneNumber: number;
  password: string;
}

interface IAccountObject {
  name: string;
  noOfConnectedUsers: number;
  noOfConcurrentUsers: number;
  noOfImsis: number;
  licenseStartDate: Date;
  licenseEndDate: Date;
  subscriptionPlan: string;
  storage: string; // e.g'2GB',
  internetBandwidth: string;
  noOfLogfiles: number;
  noOfSites: number;
  noOfKits: number;
}

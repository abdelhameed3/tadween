import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "./../../services/auth.service";
import { HelperService } from "./../../services/helper.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-verify-account",
  templateUrl: "./verify-account.component.html",
  styleUrls: ["./verify-account.component.scss"],
})
export class VerifyAccountComponent implements OnInit {
  code;
  phone;
  constructor(
    private helper: HelperService,
    private auth: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.phone = this.activatedRoute.snapshot.params.phone;
  }
  async verifyAccount() {
    try {
      const body = {
        phone: this.phone,
        verification_code: this.code,
      };
      console.log(body);
      const res: any = await this.auth.verify(body);
      console.log(res.data);
      if (res.status) {
        this.helper.successAlert(res.message);
        this.router.navigate(["/user/dashboard"]);
        localStorage.setItem("token", "Bearer " + res.data.api_token);
        localStorage.setItem("user", res.data.user);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

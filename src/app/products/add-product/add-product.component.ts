import { environment } from "./../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";
import { ProductService } from "./../../services/product.service";
import { HelperService } from "./../../services/helper.service";
import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"],
})
export class AddProductComponent implements OnInit {
  publicUrl = environment.publicUrl;

  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  products = {} as any;
  productId;
  constructor(
    private sanitizer: DomSanitizer,
    private helper: HelperService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.productId = this.activatedRoute.snapshot.params.id;
    if (this.productId) {
      this.getProductById();
    }
  }
  uploadFile($event) {
    console.log($event);
    this.fileData = $event.target.files[0];
    this.fileChanged = true;
    this.uploadedFile = this.sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(this.fileData)
    );
  }

  clearFile() {
    this.fileData = null;
    this.uploadedFile = null;
  }

  async addProduct() {
    console.log(this.products);
    try {
      this.products.photo = this.fileData;
      const formData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in this.products) {
        formData.append(key, this.products[key]);
      }
      const res: any = await this.productService.add(formData);
      if (res.status) {
        this.helper.successAlert(res.message);
        this.products = {};
        this.fileData = null;
        this.uploadedFile = null;
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async editProduct() {
    console.log(this.fileChanged);
    try {
      if (!this.fileChanged) {
        delete this.products.photo;
      } else {
        this.products.photo = this.fileData;
      }
      if (this.fileData == null) {
        this.products.delete_photo = true;
      }
      this.products._method = "put";
      const formData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in this.products) {
        formData.append(key, this.products[key]);
      }
      const res: any = await this.productService.edit(this.productId, formData);
      if (res.status) {
        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async getProductById() {
    try {
      const res: any = await this.productService.getById(this.productId);

      if (res.status) {
        this.uploadedFile = this.publicUrl + res.data.photo;
        this.products = res.data;
        this.fileData = this.publicUrl + res.data.photo;
      } else {
        this.helper.errorAlert(res.message);
        this.router.navigate(["/user/list-products"]);
      }
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  }
}

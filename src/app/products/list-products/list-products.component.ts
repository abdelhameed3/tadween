import { ModalDirective } from "ngx-bootstrap/modal";
import { ProductService } from "./../../services/product.service";
import { HelperService } from "./../../services/helper.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { environment } from "./../../../environments/environment";

@Component({
  selector: "app-list-products",
  templateUrl: "./list-products.component.html",
  styleUrls: ["./list-products.component.scss"],
})
export class ListProductsComponent implements OnInit {
  @ViewChild("Image") Image: ModalDirective;

  modalImage;
  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  storeID = JSON.parse(localStorage.getItem("user")).store_id;
  products;
  publicUrl = environment.publicUrl;

  constructor(
    private helper: HelperService,
    private productService: ProductService
  ) {}

  ngOnInit() {
    this.getProduct();
  }

  async getProduct() {
    try {
      const res: any = await this.productService.get(this.storeID);
      if (res.status) {
        this.products = res.data;
      } else {
        this.helper.errorAlert(res.massage);
      }
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  }
  showImage(src) {
    console.log(src);
    this.modalImage = src;
    this.Image.show();
  }
}

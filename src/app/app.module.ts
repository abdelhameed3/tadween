// Angular Core
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  BrowserModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG,
} from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RouterModule } from "@angular/router";

// Routing
import { AppRoutes } from "./app.routing";
import { AppComponent } from "./app.component";

// Layouts
import {
  CondensedComponent,
  BlankComponent,
  RootLayout,
} from "./@pages/layouts";
// Layout Service - Required
import { pagesToggleService } from "./@pages/services/toggler.service";

// Shared Layout Components
import { SidebarComponent } from "./@pages/components/sidebar/sidebar.component";
import { QuickviewComponent } from "./@pages/components/quickview/quickview.component";
import { QuickviewService } from "./@pages/components/quickview/quickview.service";
import { SearchOverlayComponent } from "./@pages/components/search-overlay/search-overlay.component";
import { HeaderComponent } from "./@pages/components/header/header.component";
import { HorizontalMenuComponent } from "./@pages/components/horizontal-menu/horizontal-menu.component";
import { SharedModule } from "./@pages/components/shared.module";
import { pgListViewModule } from "./@pages/components/list-view/list-view.module";
import { pgCardSocialModule } from "./@pages/components/card-social/card-social.module";
import { MessageModule } from "./@pages/components/message/message.module";
import { MessageService } from "./@pages/components/message/message.service";
import { DropdownModule } from "primeng/dropdown";
import { AgmCoreModule } from "@agm/core";

// Basic Bootstrap Modules
import {
  BsDropdownModule,
  AccordionModule,
  AlertModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  ProgressbarModule,
  TabsModule,
  TooltipModule,
  TypeaheadModule,
} from "ngx-bootstrap";

// Pages Globaly required Components
import { pgTabsModule } from "./@pages/components/tabs/tabs.module";
import { pgSwitchModule } from "./@pages/components/switch/switch.module";
import { ProgressModule } from "./@pages/components/progress/progress.module";
import { CalendarModule } from "primeng/calendar";

// Thirdparty Components / Plugins
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { NgxEchartsModule } from "ngx-echarts";

// Modules
import { UserModule } from "./user.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { AuthInterceptor } from "./services/auth-interceptor";
import { ListUserComponent } from "./users/list-user/list-user.component";
import { AddUserComponent } from "./users/add-user/add-user.component";
import { AddInvoiceComponent } from "./invoices/add-invoice/add-invoice.component";
import { ListInvoiceComponent } from "./invoices/list-invoice/list-invoice.component";
import { ListClientsComponent } from "./clients/list-clients/list-clients.component";
import { AddClientComponent } from "./clients/add-client/add-client.component";
import { AddProductComponent } from "./products/add-product/add-product.component";
import { ListProductsComponent } from "./products/list-products/list-products.component";
import { AddRepresentativeComponent } from "./representatives/add-representative/add-representative.component";
import { ListRepresentativesComponent } from "./representatives/list-representatives/list-representatives.component";
import { ProfileComponent } from "./users/profile/profile.component";
import { pgSelectModule } from "./@pages/components/select/select.module";
import { SubscriptionComponent } from "./subscription/subscription/subscription.component";
import { AddVoucherComponent } from "./vouchers/add-voucher/add-voucher.component";
import { ListVouchersComponent } from "./vouchers/list-vouchers/list-vouchers.component";
import { RegisterComponent } from "./register/register.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { VerifyAccountComponent } from "./register/verify-account/verify-account.component";
import { RepresentativesSettingsComponent } from './representatives/representatives-settings/representatives-settings.component';
import { SettingsComponent } from './users/settings/settings.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

// Hammer Config Overide
// https://github.com/angular/angular/issues/10541
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    pinch: { enable: false },
    rotate: { enable: false },
  };
}

@NgModule({
  declarations: [
    AppComponent,
    RootLayout,
    CondensedComponent,
    BlankComponent,
    SidebarComponent,
    QuickviewComponent,
    SearchOverlayComponent,
    HeaderComponent,
    HorizontalMenuComponent,
    ListUserComponent,
    AddUserComponent,
    AddInvoiceComponent,
    ListInvoiceComponent,
    ListClientsComponent,
    AddClientComponent,
    AddProductComponent,
    ListProductsComponent,
    AddRepresentativeComponent,
    ListRepresentativesComponent,
    ProfileComponent,
    SubscriptionComponent,
    AddVoucherComponent,
    ListVouchersComponent,
    RegisterComponent,
    VerifyAccountComponent,
    RepresentativesSettingsComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    ProgressModule,
    pgListViewModule,
    pgCardSocialModule,
    DropdownModule,
    pgSelectModule,
    CalendarModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    pgTabsModule,
    PerfectScrollbarModule,
    pgSwitchModule,
    NgxEchartsModule,
    UserModule,
    DashboardModule,
    MessageModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyD6YcM1P1-Is14NjL2JLIHZ9It5xBDTnfI",
    }),
  ],
  providers: [
    QuickviewService,
    pagesToggleService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: AppHammerConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../@pages/components/message/message.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  public changePasswordForm: FormGroup;
  public resetPasswordId: string;

  constructor(
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _messageService: MessageService,
    private route: ActivatedRoute,
  ) {}

  changePassword() {
    const password = this.changePasswordForm.controls.password.value;
    this._authService
      .changePassword(password, this.resetPasswordId)
      .toPromise()
      .then(res => {
        this._router.navigate(['/']);
        this._messageService.create('success', res.msg, {
          Title: 'Success',
          imgURL: 'assets/img/profiles/avatar.jpg',
          Position: 'top-right',
          Style: 'circle',
          Duration: 4000,
        });
      })
      .catch(err => {
        console.log({ err });
        this._messageService.create('error', err.error.msg, {
          Title: 'Error',
          imgURL: 'assets/img/profiles/avatar.jpg',
          Position: 'top-right',
          Style: 'circle',
          Duration: 4000,
        });
      });
  }

  public ngOnInit() {
    this.route.params.subscribe(params => {
      this.resetPasswordId = params['resetPasswordId'];
    });
    this.changePasswordForm = this._formBuilder.group({
      password: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '/((?=.*d)|(?=.*w+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/',
          ),
        ],
      ],
    });
  }
}

import { SettingsComponent } from "./users/settings/settings.component";
import { VerifyAccountComponent } from "./register/verify-account/verify-account.component";
import { RegisterComponent } from "./register/register.component";
import { AddVoucherComponent } from "./vouchers/add-voucher/add-voucher.component";
import { ListVouchersComponent } from "./vouchers/list-vouchers/list-vouchers.component";
import { SubscriptionComponent } from "./subscription/subscription/subscription.component";
import { ProfileComponent } from "./users/profile/profile.component";
import { ListRepresentativesComponent } from "./representatives/list-representatives/list-representatives.component";
import { AddRepresentativeComponent } from "./representatives/add-representative/add-representative.component";
import { AddProductComponent } from "./products/add-product/add-product.component";
import { ListProductsComponent } from "./products/list-products/list-products.component";
import { AddClientComponent } from "./clients/add-client/add-client.component";
import { ListClientsComponent } from "./clients/list-clients/list-clients.component";
import { AddInvoiceComponent } from "./invoices/add-invoice/add-invoice.component";
import { ListInvoiceComponent } from "./invoices/list-invoice/list-invoice.component";
import { AddUserComponent } from "./users/add-user/add-user.component";
import { ListUserComponent } from "./users/list-user/list-user.component";
import { Routes } from "@angular/router";

import { BlankComponent } from "./@pages/layouts/blank/blank.component";
import { CondensedComponent } from "./@pages/layouts";
import { LoginComponent } from "./login/login.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AuthGuard } from "./guards/auth.guard";
import { RepresentativesSettingsComponent } from "./representatives/representatives-settings/representatives-settings.component";

export const AppRoutes: Routes = [
  {
    path: "",
    component: BlankComponent,
    children: [
      {
        path: "",
        component: LoginComponent,
      },
      {
        path: "verify-account/:phone",
        component: VerifyAccountComponent,
      },
      {
        path: "login",
        component: LoginComponent,
      },
      {
        path: "register",
        component: RegisterComponent,
      },
      {
        path: "reset-password",
        component: ResetPasswordComponent,
      },
      {
        path: "change-password/:resetPasswordId",
        component: ChangePasswordComponent,
      },
    ],
  },
  {
    path: "user",
    component: CondensedComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
        data: {
          breadcrumb: "Dashboard",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "profile",
        component: ProfileComponent,
        data: {
          breadcrumb: "Dashboard",
        },
        canActivate: [AuthGuard],
      },

      {
        path: "list-user",
        component: ListUserComponent,
        data: {
          breadcrumb: "List User",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-user",
        component: AddUserComponent,
        data: {
          breadcrumb: "Add User",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "list-invoices",
        component: ListInvoiceComponent,
        data: {
          breadcrumb: "List Invoices",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-invoice",
        component: AddInvoiceComponent,
        data: {
          breadcrumb: "Add Invoice",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "edit-invoice/:id",
        component: AddInvoiceComponent,
        data: {
          breadcrumb: "Add Invoice",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "list-clients",
        component: ListClientsComponent,
        data: {
          breadcrumb: "List Clients",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-client",
        component: AddClientComponent,
        data: {
          breadcrumb: "Add Client",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "list-products",
        component: ListProductsComponent,
        data: {
          breadcrumb: "List Products",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-product",
        component: AddProductComponent,
        data: {
          breadcrumb: "Add Product",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "edit-product/:id",
        component: AddProductComponent,
        data: {
          breadcrumb: "Edit Product",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "list-representatives",
        component: ListRepresentativesComponent,
        data: {
          breadcrumb: "List Representative",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-representative",
        component: AddRepresentativeComponent,
        data: {
          breadcrumb: "Add Representative",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "representative-settings/:id",
        component: RepresentativesSettingsComponent,
        data: {
          breadcrumb: "Add Representative",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "subscription",
        component: SubscriptionComponent,
        data: {
          breadcrumb: "Add Subscription",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "settings",
        component: SettingsComponent,
        data: {
          breadcrumb: "Settings",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "list-vouchers",
        component: ListVouchersComponent,
        data: {
          breadcrumb: "List Vouchers",
        },
        canActivate: [AuthGuard],
      },
      {
        path: "add-voucher",
        component: AddVoucherComponent,
        data: {
          breadcrumb: "Add Voucher",
        },
        canActivate: [AuthGuard],
      },
    ],
  },
];

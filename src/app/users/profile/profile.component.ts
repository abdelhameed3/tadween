import { HelperService } from "./../../services/helper.service";
import { environment } from "./../../../environments/environment";
import { ProfileService } from "./../../services/profile.service";
import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { MouseEvent } from "@agm/core";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  uploadedFile: any;
  public fileData: any = null;
  fileChanged = false;
  countries: any[];
  selectedCountry: any;
  publicUrl = environment.publicUrl;

  userId;
  mapCenter = {} as any;
  lat;
  lng;
  zoom = 15;
  markers: marker[] = [];

  userData = {} as any;
  constructor(
    private sanitizer: DomSanitizer,
    private helper: HelperService,
    private profileService: ProfileService
  ) {}

  ngOnInit() {
    this.countries = [
      { name: "Australia", code: "AU" },
      { name: "Brazil", code: "BR" },
      { name: "China", code: "CN" },
      { name: "Egypt", code: "EG" },
      { name: "France", code: "FR" },
      { name: "Germany", code: "DE" },
      { name: "India", code: "IN" },
      { name: "Japan", code: "JP" },
      { name: "Spain", code: "ES" },
      { name: "United States", code: "US" },
    ];

    this.userId = JSON.parse(localStorage.getItem("user")).id;
    this.getUser();
  }

  uploadFile($event) {
    console.log($event);
    this.fileData = $event.target.files[0];
    this.fileChanged = true;
    this.uploadedFile = this.sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(this.fileData)
    );
  }

  clearFile() {
    this.fileData = null;
    this.uploadedFile = null;
  }
  async getUser() {
    try {
      const res: any = await this.profileService.get(this.userId);
      if (res.status) {
        this.userData = res.data;
        console.log(this.userData);
        this.markers.push({
          lat: res.data.latitude,
          lng: res.data.longitude,
          draggable: true,
        });

        this.mapCenter = {
          lat: +res.data.latitude,
          lng: +res.data.longitude,
        };
        console.log(this.markers);
        console.log(this.mapCenter);
        this.uploadedFile = res.data.photo;
      }
    } catch (err) {
      console.log(err);
    }
  }
  async updateUser() {
    try {
      delete this.userData.query_code;
      delete this.userData.type;
      delete this.userData.user_type_name;
      delete this.userData.email_verified;
      delete this.userData.completed;
      delete this.userData.store_id;
      delete this.userData.is_subscribed;
      delete this.userData.subscription;
      delete this.userData.stores;
      delete this.userData.categoryText;

      this.userData.latitude = this.lat + "";
      this.userData.longitude = this.lng + "";
      if (this.fileChanged) {
        this.userData.photo = this.fileData;
      } else {
        delete this.userData.photo;
      }
      const formData = this.helper.jsonToFormData(this.userData);
      const res: any = await this.profileService.update(formData);
      if (res.status) {
        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
      }
      console.log(this.userData);
    } catch (err) {
      console.log(err);
    }
  }
  mapClicked($event: MouseEvent) {
    this.markers = [
      {
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        draggable: true,
      },
    ];
    this.lat = this.markers[0].lat;
    this.lng = this.markers[0].lng;
    console.log(this.lat, this.lng);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log("dragEnd", m, $event);
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

export const environment = {
    production: true,
    environmentName: 'staging',
    apiUrl: 'http://172.104.152.254:4000',
    staticURL : 'https://stage.v2.static.inos.lyticshub.com'
  };

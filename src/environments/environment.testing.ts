
  export const environment = {
    production: true,
    environmentName: 'testing',
    apiUrl: 'https://test.v2.api.inos.lyticshub.com',
    staticURL : 'https://test.v2.static.inos.lyticshub.com'
  };
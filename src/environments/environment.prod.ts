export const environment = {
  production: true,
  environmentName: "production",
  apiUrl: "https://live.tadween.app/api",
  publicUrl: "https://live.tadween.app",
};
